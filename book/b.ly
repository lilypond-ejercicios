\book {

      \bookpart {
      		\score {
		       { c' }		
		       \paper { indent= 6\cm }
		}
      		\score {
		       { d' }		
		       \paper { indent= 6\cm }
		}

      }

      \bookpart {
      		\score {
		       { e' }		
		}

      		\score {
		       { f' }		
		}
      }


}

\book {
      \bookpart {
      		\score {
		       { g' }		
		}
      		\score {
		       { a' }		
		}

      }
      \bookpart {
      		\score {
		       { b' }		
		}

      		\score {
		       { c'' }		
		}
      }
\paper { indent= 6\cm }
}

